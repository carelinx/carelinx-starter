const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');
const compression = require('compression');
const { createReadStream } = require('fs');
const routeMasking = require('./route-masking');

const dev = process.env.NODE_ENV !== 'production';
const port = process.env.PORT || 3003;
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();
    const httpServer = require('http').Server(server);

    server.use(compression());
    server.use(bodyParser.urlencoded({ extended: true }));
    server.use(bodyParser.json());

    // dynamic URLS
    routeMasking(app, server);

    server.get('*', (req, res) => handle(req, res));

    httpServer.listen(port, err => {
      if (err) throw err;
      console.log('> Custom server ready on http://localhost:' + port);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
