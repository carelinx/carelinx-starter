import css from 'styled-jsx/css';

export const COLORS = {
  orange: '#ff6633',
  blue: '#4a90e2',
  blueHover: '#386cab',
  textRegular: '#646464',
  textBlack: '#222222',
  textGray: '#9D9D9D',
  borderGray: '#CCC',
  grayBackground: '#f9f9f9',
  grayBackgroundDark: '#f2f2f2',
  yellowBackground: '#FFCC66',
  blackBackground: '#202020'
};

export const SCREEN_SIZE = {
  small: '767px',
  medium: '992px',
  large: '1200px'
};

/*language=CSS*/
export default css.global`
  html,
  body {
    font-family: Lato, 'Helvetica Neue', Helvetica, sans-serif !important;
    font-size: 16px !important;
    font-weight: 400 !important;
    color: ${COLORS.textRegular} !important;
    margin: 0;
    padding: 0;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
  }

  * {
    transition: color 150ms, background-color 150ms, box-shadow 150ms;
  }

  .hover,
  .link {
    cursor: pointer;
  }

  .box {
    padding: 25px 30px;
    background-color: white;
    border-radius: 4px;
    box-shadow: 0px 2px 6px 1px hsla(0, 0%, 0%, 0.2);
  }

  .box-light {
    padding: 25px 30px;
    background-color: white;
    border-radius: 4px;
    box-shadow: 0px 2px 6px 1px hsla(0, 0%, 0%, 0.02);
  }

  h1,
  h2,
  h3,
  h4 {
    color: ${COLORS.textRegular} !important;
  }

  form h1,
  form h2,
  form h3,
  form h4 {
    margin-top: 0;
  }

  input:focus,
  select:focus,
  textarea:focus,
  button:focus {
    outline: none !important;
  }

  form label, label {
    font-weight: 400 !important;
  }

  .btn {
    text-transform: uppercase;
    font-weight: 400 !important;
  }

  .btn-primary {
    background-color: ${COLORS.blue} !important;
    border-color: ${COLORS.blue} !important;
  }

  .btn-primary:hover {
    background-color: ${COLORS.blueHover} !important;
    border-color: ${COLORS.blueHover} !important;
  }

  .btn-default {
    color: #b5b5b5 !important;
  }

  .btn,
  .form-control {
    height: 38px !important;
    border-radius: 18px !important;
    padding: 6px 18px !important;
    font-size: 16px !important;
  }
  
  textarea.form-control {
    height: auto !important;
  }

  .form-control:focus {
    border-color: ${COLORS.blue} !important;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
      0 0 1px rgba(56, 108, 171, 0.6) !important;
  }

  .form-control:hover {
    border-color: ${COLORS.blue} !important;
  }

  .has-error .form-control:focus, 
  .has-error .form-control:hover {
    border-color: #843534 !important;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
      0 0 1px rgba(132,53,52,0.6) !important;
  }
  
  .gray-background,
  .grey-background {
    background-color: ${COLORS.grayBackground};
  }
  
  .gray-background-dark,
  .grey-background-dark {
    background-color: ${COLORS.grayBackgroundDark};
  }

  .yellow-background {
    background-color: ${COLORS.yellowBackground};
  }

  .margin-top {
    margin-top: 20px;
  }

  .margin-bottom {
    margin-bottom: 20px;
  }

  .margin-left {
    margin-left: 20px;
  }

  .margin-right {
    margin-right: 20px;
  }
  
  .text-light {
    font-weight: lighter;
  }

  .text-blue {
    color: ${COLORS.blue} !important;
  }
  
  .text-blue.link:hover {
    color: ${COLORS.blueHover} !important;
  }

  .text-gray {
    color: ${COLORS.textGray} !important;
  }

  .text-display-box {
    background-color: ${COLORS.grayBackground};
    border: solid 1px ${COLORS.borderGray};
    display: inline-block;
    padding: 4px 10px;
    border-radius: 4px;
    font-size: 0.9em;
  }
  
  .button-group {
    text-align: right;
  } 
     
  .button-group button, 
  .button-group .btn 
  {
    margin-right: 20px;
  }
  
  .button-group button:last-of-type, 
  .button-group .btn:last-of-type 
  {
    margin-right: 0;
  }
`;
