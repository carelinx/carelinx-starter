import React from 'react';
import Link from 'next/link';
import { connect } from 'react-redux';

class Index extends React.Component {
  static isPublic = true;

  render() {
    return (
      <div className="container">
        <h1>Hello World</h1>
        <p>Welcome to CareLinx Starter</p>
        <Link href="/ui-demo">
          <a>UI Demo</a>
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps)(Index);
