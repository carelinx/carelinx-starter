import axios from 'axios';
import Router from 'next/router';
import getConfig from 'next/config';
import axiosOptions from '../utils/axios-utils';

const { publicRuntimeConfig } = getConfig();
const { API_URL } = publicRuntimeConfig;

export function login(payload, next = '/') {
  return dispatch =>
    axios
      .post(`${API_URL}/v1/web/accounts/login/`, payload, axiosOptions())
      .then(resp => {
        dispatch(whoAmI()).then(user => {
          if (user) {
            Router.push(next);
          }
        });
        return resp;
      })
      .catch(err => err.response);
}

export function logout() {
  return dispatch =>
    axios
      .get(`${API_URL}/v1/web/accounts/logout/`)
      .then(resp => {
        dispatch({ type: 'LOGOUT' });
        Router.push('/');
      })
      .catch(err => err.response);
}

export function whoAmI(cookie) {
  return dispatch => {
    const headers = {
      Accept: 'application/json'
    };
    if (cookie) {
      headers.Cookie = cookie;
    }
    return axios
      .get(`${API_URL}/v1/whoami/`, {
        headers: headers,
        withCredentials: true
      })
      .then(response => {
        let user = null;
        if (response.data) {
          user = response.data.data;
        }
        dispatch({
          type: 'SET_USER',
          user
        });
        return user;
      })
      .catch(err => {
        return null;
      });
  };
}
