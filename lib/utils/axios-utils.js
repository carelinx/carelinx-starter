import Cookies from 'js-cookie';

export default function axiosOptions() {
  const headers = { Accept: 'application/json' };
  const csrftoken = Cookies.get('clinxcsrf');
  if (csrftoken) {
    headers['X-CSRFTOKEN'] = csrftoken;
  }
  return {
    headers
  };
}
