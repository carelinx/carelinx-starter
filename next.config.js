const NODE_ENV = process.env.NODE_ENV;
let API_URL = 'http://localapi.carelinx.com';

if (NODE_ENV === 'production') {
  API_URL = 'https://api.carelinx.com';
}

module.exports = {
  // WARNING: variables here are sent to the client. Only expose public keys
  publicRuntimeConfig: {
    NODE_ENV: NODE_ENV,
    API_URL: API_URL
  }
};
