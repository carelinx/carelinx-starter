import React from 'react';
import PropTypes from 'prop-types';
import { COLORS } from '../../../styles/global-styles';
import CareLinxHeart from '../icons/CareLinxHeart';
import { getBrowserInfo } from '../../../lib/utils/browser-detection';

class Loading extends React.Component {
  static propTypes = {
    width: PropTypes.number
  };

  state = {
    spinnerImg: ''
  };

  componentDidMount() {
    let spinnerImg = "url('/static/spinner.svg') no-repeat";
    if (getBrowserInfo().indexOf('IE ') === 0) {
      spinnerImg = "url('/static/spinner.gif') no-repeat";
    }
    this.setState({ spinnerImg });
  }

  render() {
    let width = this.props.width || 100;
    if (width < 50) {
      // min width=50
      width = 50;
    }

    return (
      <div className="loading">
        <div className="spinner" />
        <div className="heart">
          <CareLinxHeart color={COLORS.orange} width={width / 2} />
        </div>
        <style jsx>{/*language=CSS*/
        `
          .loading {
            position: relative;
            width: ${width}px;
            height: ${width}px;
            opacity: 0.5;
          }

          .spinner {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background: ${this.state.spinnerImg};
            background-size: ${width}px ${width}px;
          }

          .heart {
            position: absolute;
            left: ${width / 4}px;
            top: ${width / 4}px;
          }
        `}</style>
      </div>
    );
  }
}

export default Loading;
