import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { COLORS } from '../../../styles/global-styles';

class Tooltip extends Component {
  static propTypes = {
    content: PropTypes.any.isRequired,
    placement: PropTypes.oneOf(['top', 'bottom', 'left', 'right'])
  };

  static defaultProps = {
    placement: 'top'
  };

  render() {
    const { content, children, placement } = this.props;

    return (
      <div className="xtooltip">
        <div className={'xtooltip-content tooltip-' + placement}>{content}</div>
        {children}
        <style jsx>{/*language=CSS*/
        `
          .xtooltip {
            position: relative;
            display: inline-block;
            border-bottom: 1px dotted ${COLORS.textBlack}; /* If you want dots under the hoverable text */
          }

          .xtooltip-content {
            visibility: hidden;
            width: 120px;
            background-color: ${COLORS.blackBackground};
            color: #fff;
            text-align: center;
            padding: 5px 0;
            border-radius: 6px;

            /* Position the tooltip text */
            position: absolute;
            z-index: 1;

            /* Fade in tooltip */
            opacity: 0;
            transition: opacity 0.3s;
          }

          .tooltip-top {
            bottom: 125%;
            left: 50%;
            margin-left: -60px;
          }

          .tooltip-right {
            top: -5px;
            left: 125%;
          }

          .tooltip-bottom {
            top: 135%;
            left: 50%;
            margin-left: -60px;
          }

          .tooltip-left {
            top: -5px;
            bottom: auto;
            right: 128%;
          }

          /* arrow */
          .tooltip-top::after {
            content: '';
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: ${COLORS.blackBackground} transparent transparent
              transparent;
          }

          .tooltip-right::after {
            content: '';
            position: absolute;
            top: 50%;
            right: 100%;
            margin-top: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: transparent ${COLORS.blackBackground} transparent
              transparent;
          }

          .tooltip-bottom::after {
            content: '';
            position: absolute;
            bottom: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: transparent transparent ${COLORS.blackBackground}
              transparent;
          }

          .tooltip-left::after {
            content: '';
            position: absolute;
            top: 50%;
            left: 100%;
            margin-top: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: transparent transparent transparent
              ${COLORS.blackBackground};
          }

          .xtooltip:hover .xtooltip-content {
            visibility: visible;
            opacity: 1;
          }
        `}</style>
      </div>
    );
  }
}

export default Tooltip;
