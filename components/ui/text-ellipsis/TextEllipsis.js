import React from 'react';
import PropTypes from 'prop-types';

const DEFAULT_LINE_HEIGHT = 22.85; // in px, from bootstrap

const TextEllipsis = props => (
  <div className="text-ellipsis">
    {props.children}
    <style jsx="">{/*language=CSS*/
    `
      .text-ellipsis {
        height: ${DEFAULT_LINE_HEIGHT * props.lineClamp}px;
        display: -webkit-box;
        -webkit-line-clamp: ${props.lineClamp};
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    `}</style>
  </div>
);

TextEllipsis.propTypes = {
  lineClamp: PropTypes.number.isRequired
};

TextEllipsis.defaultProps = {
  lineClamp: 1
};

export default TextEllipsis;
