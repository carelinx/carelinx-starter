import React from 'react';
import { render, cleanup } from 'react-testing-library';
import Checkbox from './Checkbox';

describe('Checkbox test', () => {
  afterEach(cleanup);

  it('should render', function() {
    const label = 'I am a checkbox';
    const result = render(<Checkbox label={label} />);
    const element = result.getByText(label);
    expect(element).not.toBeNull();
  });
});
