import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { COLORS } from '../../../styles/global-styles';

class Checkbox extends Component {
  static propTypes = {
    label: PropTypes.string,
    checked: PropTypes.bool,
    onChange: PropTypes.func
  };

  render() {
    const { checked, onChange } = this.props;
    return (
      <div className={'xcheckbox ' + (checked ? 'checked' : '')}>
        <label className="xlabel">
          <input
            type="checkbox"
            className="xcontrol"
            checked={checked}
            onChange={onChange}
            data-testid={this.props['data-testid']}
          />
          {this.props.label || ' '}
          <span className="xcheckmark" />
        </label>
        <style jsx>{/*language=CSS*/
        `
          .xcheckbox {
            display: inline-block;
            position: relative;
          }

          .xcontrol {
            opacity: 0;
            width: 0;
            height: 0;
          }

          .xlabel {
            margin-bottom: 0 !important;
            user-select: none;
            cursor: pointer;
          }

          .xlabel:before {
            content: ' ';
            width: 25px;
            height: 25px;
            border: solid 1px ${COLORS.borderGray};
            background-color: ${COLORS.grayBackground};
            display: inline-block;
            vertical-align: middle;
            margin-right: 10px;
            border-radius: 5px;
            position: relative;
            top: -2px;
            transition: color 150ms, background-color 150ms, box-shadow 150ms,
              border-color 150ms;
          }

          .xlabel:hover:before,
          .xlabel:focus:before {
            border-color: ${COLORS.blue};
          }

          .xcheckbox.checked .xlabel:before {
            background-color: ${COLORS.blue};
            border-color: ${COLORS.blue};
          }

          .xcheckbox.checked .xlabel:hover:before,
          .xcheckbox.checked .xlabel:focus:before {
            background-color: ${COLORS.blueHover};
            border-color: ${COLORS.blueHover};
          }

          .xcheckmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
          }

          .xcheckmark:after {
            content: '';
            position: absolute;
            left: 10px;
            top: 3px;
            width: 6px;
            height: 13px;
            border: solid white;
            border-width: 0 2px 2px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
            display: none;
          }

          .xcheckbox.checked .xcheckmark:after {
            display: block;
          }
        `}</style>
      </div>
    );
  }
}

export default Checkbox;
