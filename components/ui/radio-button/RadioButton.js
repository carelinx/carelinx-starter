import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { COLORS } from '../../../styles/global-styles';

class RadioButton extends Component {
  static propTypes = {
    options: PropTypes.array,
    direction: PropTypes.oneOf(['horizontal', 'vertical']),
    name: PropTypes.string
  };

  static defaultProps = {
    direction: 'vertical'
  };

  render() {
    const { options, name, direction } = this.props;
    return (
      <div>
        {options.map(op => (
          <label key={op.value} className={'xcontainer ' + direction}>
            {op.label}
            <input type="radio" name={name} />
            <span className="xcheckmark" />
          </label>
        ))}
        <style jsx>{/*language=CSS*/
        `
          .xcontainer {
            display: block;
            position: relative;
            padding-left: 34px;
            margin-bottom: 12px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
          }

          /* Hide the browser's default radio button */
          .xcontainer input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
          }

          /* Create a custom radio button */
          .xcheckmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            border: solid 1px ${COLORS.borderGray};
            background-color: ${COLORS.grayBackground};
            border-radius: 50%;
          }

          /* On mouse-over, add a grey background color */
          .xcontainer:hover input ~ .xcheckmark {
            border-color: ${COLORS.blue};
          }

          /* When the radio button is checked, add a blue background */
          .xcontainer input:checked ~ .xcheckmark {
            background-color: ${COLORS.blue};
            border-color: ${COLORS.blue};
          }

          .xcontainer:hover input:checked ~ .xcheckmark {
            background-color: ${COLORS.blueHover};
            border-color: ${COLORS.blueHover};
          }

          /* Create the indicator (the dot/circle - hidden when not checked) */
          .xcheckmark:after {
            content: '';
            position: absolute;
            display: none;
          }

          /* Show the indicator (dot/circle) when checked */
          .xcontainer input:checked ~ .xcheckmark:after {
            display: block;
          }

          /* Style the indicator (dot/circle) */
          .xcontainer .xcheckmark:after {
            top: 8px;
            left: 8px;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background: white;
          }

          .xcontainer.horizontal {
            display: inline-block;
            margin-right: 20px;
          }

          .xcontainer.horizontal:last-of-type {
            margin-right: 0;
          }
        `}</style>
      </div>
    );
  }
}

export default RadioButton;
