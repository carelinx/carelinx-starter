import { render } from 'react-testing-library';
import RadioButton from './RadioButton';

describe('RadioButton component tests', () => {
  it('should render', function() {
    render(
      <RadioButton
        options={[{ value: 1, label: 'One' }, { value: 2, label: 'Two' }]}
      />
    );
  });
});
