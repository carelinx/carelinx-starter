import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Checkbox from '../checkbox/Checkbox';
import Loading from '../loading/Loading';
import { COLORS } from '../../../styles/global-styles';
import Modal from '../modal/Modal';
import ModalHeader from '../modal/ModalHeader';
import RadioButton from '../radio-button/RadioButton';
import Select from '../select/Select';
import Tooltip from '../tooltip/Tooltip';

class Demo extends Component {
  static propTypes = {};

  state = {
    checkboxChecked: false,
    modalNormal: false,
    modalHeader: false
  };

  toggleCheckbox = () =>
    this.setState({ checkboxChecked: !this.state.checkboxChecked });

  toggleModal = type => () => this.setState({ [type]: !this.state[type] });

  render() {
    return (
      <div className="container">
        <h2>UI Demo Page</h2>
        <div className="section">
          <h4>Input</h4>
          <input className="form-control" style={{ maxWidth: 200 }} />
        </div>
        <div className="section">
          <h4>Text area</h4>
          <textarea className="form-control" style={{ maxWidth: 200 }} />
        </div>
        <div className="section">
          <h4>React select</h4>
          <Select
            instanceId="select-example"
            style={{ maxWidth: 200 }}
            options={[
              { value: 1, label: 'Option 1' },
              { value: 2, label: 'Option 2' }
            ]}
          />
        </div>
        <div className="section">
          <h4>Checkbox</h4>
          <Checkbox
            label="I am a checkbox"
            checked={this.state.checkboxChecked}
            onChange={this.toggleCheckbox}
          />
        </div>
        <div className="section">
          <h4>Radio button</h4>
          <RadioButton
            name="radio"
            options={[
              { value: 1, label: 'Option 1' },
              { value: 2, label: 'Option 2' }
            ]}
          />
        </div>
        <div className="section">
          <h4>Loading</h4>
          <Loading />
        </div>
        <div className="section">
          <h4>Tooltip</h4>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              width: 400,
              justifyContent: 'space-between'
            }}
          >
            <Tooltip content={'This is the tooltip'}>Top</Tooltip>
            <Tooltip content={'This is the tooltip'} placement="right">
              Right
            </Tooltip>

            <Tooltip content={'This is the tooltip'} placement="bottom">
              Bottom
            </Tooltip>
            <Tooltip
              content={
                <div>
                  With more content <br />
                  Even React components <br />
                  <Checkbox label="yo" />
                </div>
              }
              placement="left"
            >
              Left
            </Tooltip>
          </div>
        </div>
        <div className="section">
          <h4>Modal</h4>
          <button
            className="btn btn-default"
            onClick={this.toggleModal('modalNormal')}
          >
            Simple
          </button>{' '}
          <button
            className="btn btn-default"
            onClick={this.toggleModal('modalHeader')}
          >
            With header
          </button>
          <Modal
            onHide={this.toggleModal('modalNormal')}
            visible={this.state.modalNormal}
          >
            Modal content goes here
          </Modal>
          <Modal
            onHide={this.toggleModal('modalHeader')}
            visible={this.state.modalHeader}
            size="large"
          >
            <ModalHeader>Modal with header</ModalHeader>
            Modal content goes here
          </Modal>
        </div>

        <style jsx>{/*language=CSS*/
        `
          .section {
            padding-top: 10px;
            padding-bottom: 20px;
            border-bottom: solid 1px ${COLORS.grayBackgroundDark};
          }
        `}</style>
      </div>
    );
  }
}

export default Demo;
