import React from 'react';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';
import { COLORS } from '../../../styles/global-styles';

class Select extends React.Component {
  static propTypes = {
    instanceId: PropTypes.any.isRequired
  };

  render() {
    const props = this.props;
    const extraClass = props.className || '';
    const extraStyle = props.style || {};
    return (
      <div className={'react-select-wrapper ' + extraClass} style={extraStyle}>
        <ReactSelect classNamePrefix="react-select" {...props} />
        <style jsx global>{/*language=CSS*/
        `
          .react-select-wrapper {
            display: inline-block;
            width: 100%;
          }

          .react-select__control {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            background-color: hsl(0, 0%, 100%);
            cursor: default;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            min-height: 38px;
            outline: 0 !important;
            position: relative;
            -webkit-transition: all 100ms;
            box-sizing: border-box;
            border: 1px solid #ccc !important;
            border-radius: 18px !important;
            transition: border-color 150ms !important;
          }

          .react-select__single-value {
            color: ${COLORS.textRegular} !important;
          }

          .react-select__control:hover {
            border: 1px solid ${COLORS.blue} !important;
          }

          .react-select__control:focus,
          .react-select__control--is-focused {
            border: 1px solid ${COLORS.blue} !important;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
              0 0 1px rgba(56, 108, 171, 0.6) !important;
          }

          /*.react-select__control--is-focused {*/
          /*border-bottom-left-radius: 0 !important;*/
          /*border-bottom-right-radius: 0 !important;*/
          /*}*/

          .react-select__menu {
            border: 1px solid ${COLORS.blue} !important;
            /*border-top: none !important;*/
            /*border-radius: 0 0 18px 18px !important;*/
            border-radius: 18px !important;
            overflow: hidden !important;
            /*top: 30px !important;*/
          }

          .react-select__option--is-selected {
            background-color: ${COLORS.blue} !important;
          }

          .react-select__value-container {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex: 1;
            -ms-flex: 1;
            flex: 1;
            -webkit-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding: 2px 8px 2px 15px !important;
            -webkit-overflow-scrolling: touch;
            position: relative;
            overflow: hidden;
            box-sizing: border-box;
          }

          .react-select__placeholder {
            color: hsl(0, 0%, 50%);
            margin-left: 2px;
            margin-right: 2px;
            position: absolute;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            box-sizing: border-box;
          }

          .react-select__indicators {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-align-self: stretch;
            -ms-flex-item-align: stretch;
            align-self: stretch;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-shrink: 0;
            -ms-flex-negative: 0;
            flex-shrink: 0;
            box-sizing: border-box;
          }

          .react-select__indicator-separator {
            -webkit-align-self: stretch;
            -ms-flex-item-align: stretch;
            align-self: stretch;
            background-color: hsl(0, 0%, 80%);
            margin-bottom: 8px;
            margin-top: 8px;
            width: 1px;
            box-sizing: border-box;
          }

          .react-select__dropdown-indicator {
            color: hsl(0, 0%, 80%);
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            padding: 8px;
            -webkit-transition: color 150ms;
            transition: color 150ms;
            box-sizing: border-box;
          }

          .react-select__dropdown-indicator svg {
            display: inline-block;
            fill: currentColor;
            line-height: 1;
            stroke: currentColor;
            stroke-width: 0;
          }
        `}</style>
      </div>
    );
  }
}

export default Select;
