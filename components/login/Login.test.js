import React from 'react';
import { cleanup, fireEvent } from 'react-testing-library';
import Login from './Login';
import renderWithRedux from '../../lib/test-utils/render-with-redux';
import * as actions from '../../lib/auth/actions';

describe('Login page tests', () => {
  let spy;

  beforeAll(() => {
    spy = jest.fn();
    spy = jest.spyOn(actions, 'login');
  });

  afterEach(cleanup);

  afterAll(() => {
    jest.restoreAllMocks();
  });

  it('should render', function() {
    // Example how to override initialState:
    // const { getByText, store } = renderWithRedux(<Index />, {
    //   initialState: { cities: { topCities: ['temuco'] } }
    // });
    const { getByText, getByTestId } = renderWithRedux(
      <Login />
    );
    const elem = getByText('Welcome back!');
    expect(elem.innerHTML).toBe('Welcome back!');

    const usernameInput = getByTestId('username-input');
    const passwordInput = getByTestId('password-input');
    const remembermeCheckbox = getByTestId('rememberme-checkbox');
    const loginBtn = getByTestId('login-button');

    fireEvent.change(usernameInput, { target: { value: 'user@domain.com' } });
    fireEvent.change(passwordInput, { target: { value: 'changeme' } });
    fireEvent.click(remembermeCheckbox); // toggle remember me to false
    fireEvent.click(loginBtn);

    expect(spy).toHaveBeenCalledWith(
      {
        username: 'user@domain.com',
        password: 'changeme',
        rememberMe: false
      },
      '/'
    );
  });
});
