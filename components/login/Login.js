import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import Link from 'next/link';
import { login } from '../../lib/auth/actions';
import { COLORS, SCREEN_SIZE } from '../../styles/global-styles';
import Checkbox from '../ui/checkbox/Checkbox';

class Login extends Component {
  static isPublic = true;

  state = {
    username: '',
    password: '',
    rememberMe: true
  };

  handleSubmit = e => {
    const { router } = this.props;
    const { query } = router;
    const next = query.next || '/';
    e.preventDefault();
    this.props.dispatch(login(this.state, next));
  };

  handleOnChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  toggleRememberMe = () =>
    this.setState({ rememberMe: !this.state.rememberMe });

  render() {
    const { username, password } = this.state;
    return (
      <div>
        <div className="gray-background">
          <div className="box">
            <h3>Welcome back!</h3>
            <br />
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label>Username</label>
                <input
                  type="text"
                  name="username"
                  value={username}
                  onChange={this.handleOnChange}
                  className="form-control"
                  placeholder="Email address"
                  data-testid="username-input"
                />
              </div>
              <div className="form-group">
                <label>Password</label>
                <input
                  type="password"
                  name="password"
                  value={password}
                  onChange={this.handleOnChange}
                  className="form-control"
                  placeholder="Password"
                  data-testid="password-input"
                />
              </div>
              <div className="clearfix">
                <div className="pull-left">
                  <Checkbox
                    label="Remember me"
                    checked={this.state.rememberMe}
                    onChange={this.toggleRememberMe}
                    data-testid="rememberme-checkbox"
                  />
                </div>
                <Link href="/">
                  <a className="pull-right">Can't login?</a>
                </Link>
              </div>
              <br />
              <input type="submit" value="Login" className="btn btn-primary" data-testid="login-button" />
            </form>
          </div>
        </div>
        <style jsx>{/*language=CSS*/ `
          .gray-background {
            padding: 50px;
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
          }

          label,
          input,
          .btn {
            display: block;
          }

          .box {
            width: 400px;
            margin: 0 auto;
          }

          .btn {
            width: 100%;
          }

          .btn-facebook {
            background-color: #3c5a99 !important;
            border-color: #3c5a99 !important;
            margin-bottom: 15px;
          }

          .btn-facebook:hover {
            background-color: #37518b !important;
            border-color: #37518b !important;
          }

          .fa-facebook-official {
            margin-right: 10px;
          }

          .divider {
            border-bottom: solid 1px ${COLORS.borderGray};
            position: relative;
            margin-bottom: 30px;
          }

          .divider span {
            position: relative;
            top: 10px;
            background-color: white;
            padding: 0 8px;
          }

          @media screen and (max-width: ${SCREEN_SIZE.small}) {
            .gray-background {
              padding: 15px;
            }
            .box {
              width: 90%;
              padding-left: 30px;
              padding-right: 30px;
              margin: 15px auto;
            }
          }
        `}</style>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  };
};

export default withRouter(connect(mapStateToProps)(Login));
