# CareLinx Starter

Starter React application with authentication, server side rendering and unit test setup using [Next.js](https://nextjs.org/docs/)

Uses vanilla bootstrap and font awesome 4.7

### Run in dev mode

```
$ npm run dev
```

Or in a different port:

```
$ PORT=3005 node server/index.js
```

### Build & run production

```
$ npm run build
```

```
$ npm run start
```


## Run unit tests

```
$ npm run test
```

With coverage report

```
$ npm run test:coverage
```

Coverage report is exported to coverage/lcov-report/index.html

## Adding new pages

Just create a regular react component in the `components` directory and import it from a file from the `pages` directory.
The name of the file will create the route automatically for you.

All pages need an authenticated user by default. If you need a public page (like the login page) you can set the `isPublic`
static property to `true`:

```
class MyPublicPage extends Component {
  static isPublic = true;
  ...
}
```

## Connecting to Redux

Just use the `connect` function as usual.

```
class MyConcectedPage extends Component {
  ...
}

const mapStateToProps = state => {
  ...
}

export default connect(mapStateToProps)(MyConcectedPage)
```

## Server side support

If your page needs data from the server you can use 
next.js [`getInitialProps`](https://github.com/zeit/next.js#fetching-data-and-component-lifecycle) async method.

If you dispatch redux actions with that data, it will be included in the initial server render
so it won't be re-fetched again client side:

```
class MySSRPage extends Component {
  static async getInitialProps(ctx) {
    const { reduxStore } = ctx;
    const isServer = typeof window === 'undefined';
    
    // await server side only for SEO purpose, on client side show a "Loading" message
    if (isServer) {
      await reduxStore.dispatch(fetchData());
    } else {
      reduxStore.dispatch(fetchData());
    }
  }
  ...
}
```
